﻿using System;
using static System.Console;

namespace HomeWorkNumberTwo
{
    class Program
    {
        static void Main(string[] args)
        {
            string fullName = "Вашакидзе Даниил Дмитриевич";
            byte age = 21;
            string email ="del8.40@mail.ru";
            float programmingScores = 80.5f;
            float mathScores = 50.2f;
            float physicsScores = 73.4f;
            float[] numberOfTasks = {programmingScores, mathScores, programmingScores };
            //WriteLine($"Фаминия Имя Отчество: {fullName}\nВозраст: {age}\nПочта: {email}\nБаллы по программированию: {programmingScores}\nБаллы по математике: {mathScores}\nБаллы по физике: {physicsScores}\n");
            //ReadKey();

            string writeLineFullName = ($"Фаминия Имя Отчество: {fullName}{Environment.NewLine}");
            string writeLineAge = ($"Возраст: {age}{Environment.NewLine}");
            string writeLineEmail = ($"Почта: {email}{Environment.NewLine}");
            string writeLineProgrammingScores = ($"Баллы по программированию: {programmingScores} баллов{Environment.NewLine}");
            string writeLineMathScores = ($"Баллы по математике: {mathScores} баллов{Environment.NewLine}");
            string writeLinePhysicsScores = ($"Баллы по физике: {physicsScores} баллов{Environment.NewLine}");
            //WriteLine($"{writeLineFullName}{writeLineAge}{writeLineEmail}{writeLineProgrammingScores}{writeLineMathScores}{writeLinePhysicsScores}");
            //ReadKey();

            string userDatabase = ($"{writeLineFullName}{writeLineAge}{writeLineEmail}{writeLineProgrammingScores}{writeLineMathScores}{writeLinePhysicsScores}");
            WriteLine("Task One:");
            WriteLine(userDatabase);
            ReadKey();

            float summScores;
            summScores = physicsScores + mathScores + physicsScores;

            float averageScore;
            averageScore = summScores / numberOfTasks.Length;
            WriteLine($"Общее количество баллов равно {summScores} и среднее количество баллов равно {averageScore}.");
            string formatAverageScore = string.Format("{0:f2}", averageScore);
            WriteLine($"Отформатируем количество баллов до двух значений после запятой");
            WriteLine($"Общее количество баллов равно {summScores}, и среднее количество баллов равно {formatAverageScore}.");
            ReadKey();
        }
    }
}
